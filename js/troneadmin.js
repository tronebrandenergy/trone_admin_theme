(function ($, Drupal) {


        // GRID TOGGLE for admin
        $('#grid-toggler #showgrid').click(function(e) {
          $('#grid-overlay').addClass('showgrid');
          $('#breakpoint').addClass('showgrid');
          $('#footer').addClass('abovegrid');
          $('#showgrid').removeClass('visible').addClass('hidden');
          $('#hidegrid').removeClass('hidden').addClass('visible');
          Cookies.set('togglegrid', 'true');
        });

        $('#grid-toggler #hidegrid').click(function(e) {
          $('#grid-overlay').removeClass('showgrid');
          $('#breakpoint').removeClass('showgrid');
          $('#showgrid').removeClass('hidden').addClass('visible');
          $('#footer').removeClass('abovegrid');
          $('#hidegrid').removeClass('visible').addClass('hidden');
          Cookies.remove('togglegrid')
        });

        if (Cookies.get('togglegrid') != 'true') {
          //do nothing
        } else {
          $('#grid-overlay').addClass('showgrid');
          $('#breakpoint').addClass('showgrid');
          $('#showgrid').removeClass('visible').addClass('hidden');
          $('#hidegrid').removeClass('hidden').addClass('visible');
          $('#footer').addClass('abovegrid');
        }
        // END GRID toggle
  $("#toggle-fullscreen").change(function() {
     $(".layout-node-form").toggleClass("full-screen", this.checked)
   }).change();

})(jQuery, Drupal);
